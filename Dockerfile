FROM openjdk:8-jre-alpine
LABEL maintainer="shipulmnsk@gmail.com"
ENV SPRING_DIR_PATH="/home"
ENV SPRING_PORT=7081
COPY /build/libs/*.jar /app.jar