## Service for mapping Material entity (CRUD-operations)

### Before start application:

* read json file with nodes (/resources/json/nodes.json)
* connect db (var by port application and node)
* read and execute all from (/resources/data)
* start HealthChecker - show status app ( RED,YELLOW,GREEN)
* start NodesChecker - save available ports from others nodes
* start ChangeLogChecker - synchronize apps into one current node

## REST API:

If node is not available then redirect to error/404 page with error message for UA
<br>
**Before PUT,DELETE,POST response** :

* calculate node for operation
* change port of node for request
* execute request
* add operation context in changelog by changelog controller

## Material Controller
### Save entity
#### POST /material

    Body parameters:
        required:
            - group           group material object ( id, groupName, groupCode, typeDirectory ) , groupName - not required
            - measure         unit material object (id, nameEdizm) , nameEdizm - not required
         not required:
            - name          name material
            - country   country abbreviation
####

    Body example: 
    {
        "name": "cookie",
        "country": "UA",
        "group": {
            "id": 2,
            "name": "Печенье",
            "groupCode": "04 ",
            "typeDirectory": 100
        },
        "measure": {
            "id": 9,
        "name": "КГ"
        }
    }

####

    Response parameters:
    Status code : 
        success: 200 success 
        error : 204 no-content

### Get material object

#### GET /material

    Query Params 
            required:
                - id ( number positive)             

###

    Response parameters:
    Status code : 
        success: 200 
        no content : 204 
    Returned result:
    {
        "id" : 7,
        "name": "cookie",
        "country": "UA",
        "group": {
            "id": 2,
            "name": "Печенье",
            "groupCode": "04 ",
            "typeDirectory": 100
        },
        "measure": {
            "id": 9,
        "name": "КГ"
        }
    }
#####

    Response parameters:
    Status code : 
        success: 200 

### Update  material object

#### PUT /material

    Body parameters:
        required:
            - id            unuque primary key of material
            - group           group material object ( id, groupName, groupCode, typeDirectory ) , groupName - not required
            - measure         unit material object (id, nameEdizm) , nameEdizm - not required
         not required:
            - name          name material
            - country   country abbreviation

    Body example:
    {
        "id" : 8,
        "name": "cookie-cook",
        "country": "UA",
        "group": {
            "id": 2,
            "name": "Печенье",
            "groupCode": "04 ",
            "typeDirectory": 100
        },
        "measure": {
            "id": 9,
        "name": "КГ"
        }
    }
####

    Response parameters:
    Status code : 
        success: 200 
        no content : 204

    Error via response - 204 status code

### Delete material object

#### DELETE /material

    Query Params 
            required:
                - id             

###

    Response parameters:
    Status code : 
        success: 200 
        no content : 204 
    Error via response - 204 status code

## ChangeLog Controller
### Get last row 
#### GET /log/last
    Response parameters:
    Status code : 
        success: 200 
    Returned result:
    {
        "id" : 1,
        "method": "DELETE",
        "json": "12",
        "masterPort": "6092"
    }
### Get changes from row
####  GET /log/changes
    Query Params 
            required:
                - id             
###
    Returned result:
    [
        {
            "id": 1,
            "method": "POST",
            "json": "{\"id\":37,\"name\":\"cook21\",\"country\":\"bel\",\"group\":{\"id\":2,\"name\":\"Печенье\",\"groupCode\":\"04 \",\"typeDirectory\":100},\"measure\":{\"id\":9,\"name\":\"КГ\"}}",
            "masterPort": "6092"
        },
        {
            "id": 2,
            "method": "DELETE",
            "json": "16",
            "masterPort": "6092"
        }
    ]

### Ping app method 
#### HEAD /log
    Response parameters:
    Status code : 
        success: 200 ( if available) 

## NotFoundController
### Get error message
#### GET /error/404
    Response parameters:
    Status code : 
        success: 200
    Response body : message - "404 NOT FOUND - RESOURCE ARE NOT AVAILABLE NOW....."

## CacheController
### Get error message
#### GET /cache
    Query Params 
            required:
                - id

    Response parameters:
    Status code : 
        success: 200
        no-content: 204
    
    Response body : as GET /material



