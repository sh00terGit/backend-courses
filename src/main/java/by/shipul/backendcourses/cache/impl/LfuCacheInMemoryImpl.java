package by.shipul.backendcourses.cache.impl;

import by.shipul.backendcourses.cache.CacheInMemory;

import java.util.*;

/**
 * LFU variant of cache . Works by frequency. Delete element with min freq. before
 * put new.
 *
 * @author Andrey Shipul
 * @since 20.01.2021 8:33
 */
public class LfuCacheInMemoryImpl<K, V> implements CacheInMemory<K, V> {

    /**
     * Map for save and get items
     */
    private final Map<K, Node> map;
    /**
     * Max size .cache
     */
    private final int maxSize;

    /**
     * Start constructor of cache impl.
     * Elements must be saved in LinkedHashMap
     *
     * @param capacity integer > 0 , or throw IllegalArgumentException
     */
    public LfuCacheInMemoryImpl(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("capacity <= 0");
        }
        this.maxSize = capacity;
        this.map = new LinkedHashMap<>(capacity, 1);
    }

    /**
     * Find in cache by key
     *
     * @param key
     * @return
     */
    @Override
    public V get(K key) {
        if (map.containsKey(Optional.of(key).get())) {
            Node node = map.get(key);
            return node.incrementFrequency().getValue();
        }
        return null;
    }

    /**
     * Delete from cache by key
     *
     * @param key
     */
    @Override
    public void delete(K key) {
        map.remove(Optional.of(key).get());
    }

    /**
     * Save in cache
     *
     * @param putKey
     * @param val
     */
    @Override
    public V put(K putKey, V val) {
        if (isFull()) {
            deleteMinFreqNode(putKey);
        }
        return (V) map.put(putKey, new Node(val));
    }

    /**
     * Check fullness map and delete element with minimal frequency
     * delete  oldest element with minimal freq
     *
     * @param putKey
     * @return
     */
    private boolean deleteMinFreqNode(K putKey) {
        long minFreq = Long.MAX_VALUE;
        K keyToRemove = null;
        //for delete  oldest element with minimal freq
        ListIterator<Map.Entry<K, Node>> iter =
                new ArrayList<Map.Entry<K, Node>>(map.entrySet()).listIterator(map.size());
        while (iter.hasPrevious()) {
            Map.Entry<K, Node> entry = iter.previous();
            if (Objects.equals(entry.getKey(), putKey)) { //auto-replace key
                return true;
            }
            if (minFreq >= entry.getValue().getFrequency()) {
                minFreq = entry.getValue().getFrequency();
                keyToRemove = entry.getKey();
            }
        }
        map.remove(keyToRemove);
        return true;
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean isFull() {
        return maxSize == map.size();
    }

    @Override
    public String toString() {
        return map.toString();
    }

    public void clear() {
        map.clear();
    }

    /**
     * Support class elements. Start freq is 1
     */
    final class Node {

        private final V value;
        private long frequency;

        public Node(V value) {
            this.value = value;
            this.frequency = 1;
        }

        public V getValue() {
            return value;
        }

        public long getFrequency() {
            return frequency;
        }

        public Node incrementFrequency() {
            ++frequency;
            return this;
        }

        @Override
        public String toString() {
            return "Node [value=" + value + ", frequency=" + frequency + "]";
        }

    }
}
