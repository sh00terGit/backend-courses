package by.shipul.backendcourses.cache.impl;

import by.shipul.backendcourses.cache.CacheInMemory;

import java.util.*;

/**
 * Lru model by.shipul.backendcourses.cache. Save size by delete last used element/
 *
 * @author Andrey Shipul
 * @since 02.02.2021 8:17
 */
public class LruCacheInMemoryByTimeImpl<K, V> implements CacheInMemory<K, V> {

    /**
     * Store of elements
     */
    private final Map<K, Node> map;
    private final int maxSize;

    /**
     * Start constructor of  cache impl.
     * Elements must be saved in LruHashMap
     *
     * @param capacity
     */
    public LruCacheInMemoryByTimeImpl(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("capacity <= 0");
        }
        this.maxSize = capacity;
        this.map = new LinkedHashMap<>(maxSize);
    }

    /**
     * Find
     *
     * @param key
     * @return
     */
    @Override
    public V get(K key) {
        Objects.requireNonNull(key, "key == null");
        if (map.containsKey(key)) {
            V value = map.get(key).setCurrentTimeStamp().getValue();
            return value;
        }
        return null;
    }

    /**
     * Delete
     *
     * @param key
     */
    @Override
    public void delete(K key) {
        Objects.requireNonNull(key, "key == null");
        map.remove(key);
    }

    /**
     * Add
     *
     * @param key
     * @param val
     */
    @Override
    public V put(K key, V val) {
        if (isFull()) {
            deleteMaxTimeStampNode(key);
        }
        return (V) map.put(key, new Node(val));
    }

    /**
     * Check fullness map and delete  oldest element by time use
     *
     * @param putKey
     * @return
     */
    private boolean deleteMaxTimeStampNode(K putKey) {
        Optional<Map.Entry<K, Node>> max = map.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue));
        max.ifPresent(kNodeEntry -> map.remove(kNodeEntry.getKey()));
        return max.isPresent();
    }

    public int size() {
        return map.size();
    }

    public void clear() {
        map.clear();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean isFull() {
        return maxSize == map.size();
    }

    @Override
    public String toString() {
        return map.toString();
    }


    /**
     * Support class-storage for LruCacheByTimeImpl
     */
    private final class Node implements Comparable<Node> {

        private final V value;
        /**
         * Time of last use
         */
        private long timeStamp;

        public Node(V value) {
            this.value = value;
            this.timeStamp = System.currentTimeMillis();
        }

        public V getValue() {
            return value;
        }

        public Node setCurrentTimeStamp() {
            this.timeStamp = System.currentTimeMillis();
            return this;
        }

        @Override
        public int compareTo(Node node) {
            return (timeStamp < node.timeStamp) ? 1 : ((timeStamp == node.timeStamp) ? 1 : -1);
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }
}
