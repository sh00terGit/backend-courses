package by.shipul.backendcourses.cache;

/**
 * @author Andrey Shipul
 * @since 19.01.2021 11:51
 */
public interface CacheInMemory<K, V> {

    /**
     * Find in cache
     *
     * @param key
     * @return
     */
    V get(K key) throws Exception;

    /**
     * Delete entity from cache
     *
     * @param key
     */
    void delete(K key) throws Exception;

    /**
     * Add to cache container
     *
     * @param key
     * @param val
     */
    V put(K key, V val) throws Exception;

}
