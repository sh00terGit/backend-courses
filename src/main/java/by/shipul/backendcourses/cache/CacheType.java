package by.shipul.backendcourses.cache;

/**
 * Types of variable cache
 *
 * @author Andrey Shipul
 * @since 20.01.2021 10:09
 */
public enum CacheType {

    LFU,
    LRU_WITH_TIMESTAMP
}
