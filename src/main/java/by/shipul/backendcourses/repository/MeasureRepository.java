package by.shipul.backendcourses.repository;

import by.shipul.backendcourses.entity.Measure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasureRepository extends JpaRepository<Measure, Long> {
}
