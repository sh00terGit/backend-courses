package by.shipul.backendcourses.repository;

import by.shipul.backendcourses.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialRepository extends JpaRepository<Material, Long> {
}
