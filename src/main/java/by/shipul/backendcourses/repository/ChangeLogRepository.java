package by.shipul.backendcourses.repository;

import by.shipul.backendcourses.entity.ChangeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChangeLogRepository extends JpaRepository<ChangeLog, Long> {

    @Query(value = "SELECT id,method,json,master_port FROM change_log ORDER BY id DESC LIMIT 1",
            nativeQuery = true)
    ChangeLog getLast();

    @Query(value = "SELECT id,method,json,master_port FROM change_log WHERE id > ? ORDER BY id",
            nativeQuery = true)
    List<ChangeLog> getChangesFromRow(long fromRow);
}
