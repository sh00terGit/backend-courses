package by.shipul.backendcourses.repository;

import by.shipul.backendcourses.entity.GroupMaterial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupMaterialRepository extends JpaRepository<GroupMaterial, Long> {
}
