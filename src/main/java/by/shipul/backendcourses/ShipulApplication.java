package by.shipul.backendcourses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 *  Single entry point application
 */
@SpringBootApplication
@ServletComponentScan
public class ShipulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShipulApplication.class, args);
    }

}
