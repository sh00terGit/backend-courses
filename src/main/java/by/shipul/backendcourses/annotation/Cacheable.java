package by.shipul.backendcourses.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Target method must be cover with cache
 *
 * @author Andrey Shipul
 * @since 21.01.2021 16:29
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Cacheable {

}
