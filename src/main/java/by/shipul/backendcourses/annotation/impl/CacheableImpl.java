package by.shipul.backendcourses.annotation.impl;

import by.shipul.backendcourses.cache.impl.LfuCacheInMemoryImpl;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.cache.CacheService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Implementation Cacheable annotation and mapper service
 *
 * @author Andrey Shipul
 * @since 21.01.2021 16:33
 */
@Aspect
@Slf4j
@Component
@RequiredArgsConstructor
public class CacheableImpl {

    /**
     * Instance of LRU or LFU cache
     *
     * @see LfuCacheInMemoryImpl
     */
    private final CacheService<Long,Material> cacheFactory;

    /**
     * Called cache
     */
    @Pointcut("@annotation(by.shipul.backendcourses.annotation.Cacheable)")
    public void getCacheable() {
        log.debug("I'm in pointcut @Cacheable");
    }

    /**
     * Called on method save*() which annotated by Cached
     *
     */
    @AfterReturning(value = "getCacheable() && execution(public * save*(..))", returning = "entity")
    public void saveInCacheAndDB(Material entity) throws Throwable {
        cacheFactory.getCacheInstance().put(entity.getId(), entity);
        log.info("Saved in cache = " + entity);
    }

    /**
     * Called on method update*() which annotated by Cached
     */
    @AfterReturning(value = "getCacheable() && execution(public * update*(..))", returning = "entity")
    public void updateInCacheAndDB(Material entity) throws Throwable {
        cacheFactory.getCacheInstance().put(entity.getId(), entity);
        log.info("updated in cache = " + entity);
    }

    /**
     * Called on method get*() which annotated by Cached
     *
     */
    @Around(value = "getCacheable() && execution(public * get*(..)) && args(id)")
    public Object getFromCacheOrDB(ProceedingJoinPoint joinPoint, long id) throws Throwable {
        Material material;
        if (Objects.isNull(cacheFactory.getCacheInstance().get(id))) {
            material = (Material) joinPoint.proceed();
            if (Objects.nonNull(material)) {
                cacheFactory.getCacheInstance().put(material.getId(), material);
                log.info("CacheableImpl Not found in cache ,gotten from db:" + material);
            }
        } else {
            material = cacheFactory.getCacheInstance().get(id);
            log.info("CacheableImpl found in cache :" + material);
        }
        return material;
    }

    /**
     * Called on method remove*() which annotated by Cached
     *
     */
    @AfterReturning("getCacheable() && execution(public * remove*(..)) && args(id)")
    public void removeFromCacheAndDB(long id) throws Throwable {
        cacheFactory.getCacheInstance().delete(id);
        log.info("CacheableImpl removeFromCacheAndDB id =" + id);
    }
}
