package by.shipul.backendcourses.annotation.impl;


import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.entity.EntityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Implement annotation @ChangeLogMethod
 *
 * @see by.shipul.backendcourses.annotation.ChangeLogMethod
 */
@Aspect
@Component
@RequiredArgsConstructor
public class ChangeLogMethodImpl {

    /**
     * Service for crud-operations with changeLog
     */
    private final EntityService<ChangeLog,Long> service;
    private final ObjectMapper objectMapper;

    @Value("${server.port}")
    private String portApplication;

    /**
     * Pointcut for annotation @ChangeLogMethod
     *
     * @see by.shipul.backendcourses.annotation.ChangeLogMethod
     */
    @Pointcut("@annotation(by.shipul.backendcourses.annotation.ChangeLogMethod)")
    public void getChangeLog() {

    }

    /**
     * Delete entity and add this to changeLog
     *
     * ChangeLogMethod and deleteEntityById method
     */
    @AfterReturning("getChangeLog() && execution(* deleteEntityById(..))")
    public void deleteAndChangeLog(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        if (args[0].getClass().equals(Long.class)) {
            String id = String.valueOf(args[0]);
            service.saveEntity(ChangeLog.builder()
                    .method(HttpMethod.DELETE.toString())
                    .json(id)
                    .masterPort(getMasterPort(args))
                    .build());
        }
    }

    /**
     * Update entity and add this to changeLog
     *
     * ChangeLogMethod and updateEntity method
     */
    @SneakyThrows
    @AfterReturning("getChangeLog() && execution(* updateEntity(..))")
    public void updateAndChangeLog(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        if (args[0].getClass().equals(Material.class)) {
            Material material = (Material) args[0];
            service.saveEntity(ChangeLog.builder()
                    .method(HttpMethod.PUT.toString())
                    .json(objectMapper.writeValueAsString(material))
                    .masterPort(getMasterPort(args))
                    .build());
        }
    }

    /**
     * Save entity and add this to changeLog
     *
     * ChangeLogMethod and updateEntity method
     */
    @SneakyThrows
    @AfterReturning(value = "getChangeLog() && execution(* saveEntity(..))")
    public void saveAndChangeLog(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        if (args[0].getClass().equals(Material.class)) {
            Material material = (Material) args[0];
            service.saveEntity(ChangeLog.builder()
                    .method(HttpMethod.POST.toString())
                    .json(objectMapper.writeValueAsString(material))
                    .masterPort(getMasterPort(args))
                    .build());
        }
    }

    /**
     * Search queryPort from args[1]
     *
     * @param args
     * @return port
     */
    private String getMasterPort(Object[] args) {
        Optional<String> queryPort = Optional.ofNullable((String) args[1]);
        return queryPort.orElse(portApplication);
    }
}
