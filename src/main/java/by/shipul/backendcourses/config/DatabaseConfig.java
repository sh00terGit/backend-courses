package by.shipul.backendcourses.config;

import by.shipul.backendcourses.util.ApplicationInitializerUtil;
import by.shipul.backendcourses.util.ApplicationNode;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

@Profile("!test")
@Configuration
@RequiredArgsConstructor
public class DatabaseConfig {

    private final Environment env;
    private final ApplicationNode currentNode;
    private final ArrayList<ApplicationNode> allNodes;

    @Value("${node.datasource.schema}")
    private String sql_table_file;

    @Value("${node.datasource.data}")
    private String sql_data_file;


    /**
     * Configure  dataSource .
     * Main : set schema by currentNode method
     *
     * @return dataSource
     */
    @Bean
    public DataSource dataSource() {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
            dataSource.setUrl(env.getProperty("spring.datasource.url"));
            dataSource.setUsername(env.getProperty("spring.datasource.username"));
            dataSource.setPassword(env.getProperty("spring.datasource.password"));
            dataSource.setSchema(currentNode.getSchemaReplication());
            return dataSource;
    }

    /**
     * Hand made populate db by jdbcTemplate
     *
     * @param jdbcTemplate
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @SneakyThrows
    @Bean
    @ConditionalOnProperty(name = "node.datasource.ddl-create",havingValue = "true")
    public boolean populateDB(JdbcTemplate jdbcTemplate) {
        Set<String> sqls = ApplicationInitializerUtil.getSqlStatementsForNode(currentNode,
                allNodes.size(),
                sql_table_file,
                sql_data_file);
        for (String sql : sqls) {
            jdbcTemplate.execute(sql);
        }
        return true;
    }


}
