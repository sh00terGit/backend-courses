package by.shipul.backendcourses.config;

import by.shipul.backendcourses.util.ApplicationInitializerUtil;
import by.shipul.backendcourses.util.ApplicationNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.ArrayList;

@Configuration
public class NodeConfig {

    @Value("${server.port}")
    private String portApplication;

    @Value("${node.file.path}")
    private String nodesDataFilePath;

    @Bean(name = "currentNode")
    public ApplicationNode getCurrentNode(ArrayList<ApplicationNode> allNodes) {
        ApplicationNode currentNode = allNodes.stream()
                .filter(i -> i.getPorts().toString().contains(portApplication))
                .findFirst()
                .get();
        currentNode.setCurrentPort(portApplication);
        return currentNode;
    }

    /**
     * Read start nodes from node.file.path
     *
     * @return List<ApplicationNode>
     */
    @Bean(name = "allNodes")
    public ArrayList<ApplicationNode> readAllNodes(ObjectMapper mapper) {
        return ApplicationInitializerUtil.readAllNodesFromJsonFile(mapper, nodesDataFilePath);
    }

}
