package by.shipul.backendcourses.config;

import by.shipul.backendcourses.cache.CacheType;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.cache.impl.CacheServiceImpl;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "cache")
public class CacheConfigProperties {

    private CacheType type;
    private int capacity;
}

@Configuration
class CacheConfig {

    @Bean
    public CacheServiceImpl<Long, Material> cacheService(CacheConfigProperties properties) {
        return new CacheServiceImpl<>(properties);
    }
}




