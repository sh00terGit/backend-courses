package by.shipul.backendcourses.service.entity.impl;

import by.shipul.backendcourses.annotation.Cacheable;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import by.shipul.backendcourses.repository.MaterialRepository;
import by.shipul.backendcourses.service.entity.MaterialEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Implementation of Entity service on Material objects
 *
 * @author Andrey Shipul
 * @since 21.01.2021 16:45
 */
@Service
@RequiredArgsConstructor
public class MaterialEntityServiceImpl implements MaterialEntityService {

    /**
     * Database repo or impl
     */
    private final MaterialRepository repository;

    /**
     * Get entity from db
     *
     * @param id id entity
     * @return
     */
    @Override
    @Cacheable
    @Transactional(readOnly = true)
    public Material getEntity(Long id) throws NotFoundServiceException {
        return repository.findById(id).orElseThrow(
                () -> new NotFoundServiceException("getEntity , no row with id = " + id));

    }

    /**
     * Save entity in db and give new primary key.
     * Set new key in id material
     *
     * @param material
     * @return
     */
    @Override
    @Cacheable
    public Material saveEntity(Material material) throws InternalServiceException {
        return Optional.of(repository.save(material)).orElseThrow(
                () -> new InternalServiceException("Can not be saved:" + material));
    }

    /**
     * Update in db by id . Set it id in material.
     *
     * @param material
     * @return
     */
    @Override
    @Cacheable
    public Material updateEntityById(Material material) throws InternalServiceException {
        return Optional.of(repository.save(material)).orElseThrow(
                () -> new InternalServiceException("updateEntityById with id =" + material.getId() + " material=" + material));
    }

    /**
     * Remove from db by primary key.
     *
     * @param primary
     */
    @Override
    @Cacheable
    public void removeEntity(Long primary) throws InternalServiceException {
        try {
            repository.deleteById(primary);
        } catch (Exception e) {
            throw new InternalServiceException("removeEntity with id=" + primary);
        }
    }

}
