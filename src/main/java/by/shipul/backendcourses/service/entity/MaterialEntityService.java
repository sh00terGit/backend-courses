package by.shipul.backendcourses.service.entity;

import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;

public interface MaterialEntityService extends EntityService<Material, Long> {

    /**
     * Get from db
     *
     * @param id id entity
     * @return
     */
    @Override
    Material getEntity(Long id) throws NotFoundServiceException;

    /**
     * Save in db and give new primary key.
     * Set new key in id material
     *
     * @param material
     * @return
     */
    @Override
    Material saveEntity(Material material) throws InternalServiceException;

    /**
     * Update in db by id . Set it id in material.
     *
     * @param material
     * @return
     */
    @Override
    Material updateEntityById(Material material) throws InternalServiceException;

    /**
     * Remove from db by primary key.
     *
     * @param primary
     */
    @Override
    void removeEntity(Long primary) throws InternalServiceException;

}
