package by.shipul.backendcourses.service.entity;

import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;

import java.util.List;

/**
 * Some methods for any entity service
 *
 * @author Andrey Shipul
 * @since 21.01.2021 16:44
 */
public interface EntityService<T, K> {

    /**
     * Get entity by service
     *
     * @param primary key
     * @return
     */
    T getEntity(K primary) throws NotFoundServiceException;

    /**
     * Save entity by service
     *
     * @param entity val
     * @return
     */
    T saveEntity(T entity) throws InternalServiceException;

    /**
     * Update entity by id
     *
     * @param entity  val
     * @return
     */
    T updateEntityById( T entity) throws InternalServiceException;

    /**
     * Delete entity
     *
     * @param primary
     */
    void removeEntity(K primary) throws InternalServiceException;

}
