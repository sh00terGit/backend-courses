package by.shipul.backendcourses.service.entity.impl;

import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import by.shipul.backendcourses.repository.ChangeLogRepository;
import by.shipul.backendcourses.service.entity.ChangeLogEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of Entity service on Material objects
 *
 * @author Andrey Shipul
 * @since 21.01.2021 16:45
 */
@Service
@RequiredArgsConstructor
public class ChangeLogEntityServiceImpl implements ChangeLogEntityService<ChangeLog> {

    /**
     * Database or impl
     */
    private final ChangeLogRepository repository;

    /**
     * Get from db
     *
     * @param id id entity
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public ChangeLog getEntity(Long id) throws NotFoundServiceException {
        return repository.findById(id).orElseThrow(
                () -> new NotFoundServiceException("getEntity with id=" + id));

    }

    /**
     * Save in db and give new primary key.
     * Set new key in id material
     *
     * @param entity
     * @return
     */
    @Override
    public ChangeLog saveEntity(ChangeLog entity) throws InternalServiceException {
        return Optional.of(repository.saveAndFlush(entity)).orElseThrow(
                () -> new InternalServiceException("saveEntity :" + entity));
    }

    /**
     * Update in db by id . Set it id in material.
     *
     * @param entity
     * @return
     */
    @Override
    public ChangeLog updateEntityById(ChangeLog entity) throws InternalServiceException {
        return Optional.of(repository.save(entity)).orElseThrow(
                () -> new InternalServiceException("updateEntityById with id =" + entity.getId() + " changeLog=" + entity));
    }

    /**
     * Remove from db by primary key.
     *
     * @param primary
     */
    @Override
    public void removeEntity(Long primary) throws InternalServiceException {
        try {
            repository.deleteById(primary);
        } catch (Exception e) {
            throw new InternalServiceException("removeEntity with id=" + primary);
        }
    }

    /**
     * Get last from db
     *
     * @return changeLog
     */
    @Override
    @Transactional(readOnly = true)
    public ChangeLog getLast() throws NotFoundServiceException {
        try {
            return repository.getLast();
        } catch (Exception e) {
            throw new NotFoundServiceException();
        }
    }

    /**
     * From row to max
     *
     * @param fromRow
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<ChangeLog> getChangesFromRow(long fromRow) {
        try {
            return repository.getChangesFromRow(fromRow);
        } catch (Exception e) {
            throw new NotFoundServiceException();
        }
    }

}
