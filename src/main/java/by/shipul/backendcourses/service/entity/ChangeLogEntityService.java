package by.shipul.backendcourses.service.entity;

import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;

import java.util.List;

public interface ChangeLogEntityService<T extends ChangeLog> extends EntityService<T, Long> {
    /**
     * Get from db
     *
     * @param id id entity
     * @return
     */
    @Override
    T getEntity(Long id) throws NotFoundServiceException;

    /**
     * Save in db and give new primary key.
     * Set new key in id material
     *
     * @param entity
     * @return
     */
    @Override
    T saveEntity(T entity) throws InternalServiceException;

    /**
     * Update in db by id . Set it id in material.
     *
     * @param entity
     * @return
     */
    @Override
    T updateEntityById(T entity) throws InternalServiceException;

    /**
     * Remove from db by primary key.
     *
     * @param primary
     */
    @Override
    void removeEntity(Long primary) throws InternalServiceException;

    /**
     * Get last from db
     *
     * @return changeLog
     */
    T getLast() throws NotFoundServiceException;

    /**
     * From row to max
     *
     * @param fromRow
     * @return
     */
    List<T> getChangesFromRow(long fromRow);
}
