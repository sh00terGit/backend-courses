package by.shipul.backendcourses.service.cache;

import by.shipul.backendcourses.cache.CacheInMemory;
import by.shipul.backendcourses.cache.CacheType;

public interface CacheService<K,V> {
    CacheInMemory<K,V> getCacheInstance();

    CacheInMemory<K,V> newInstanceCache(CacheType cacheType, int capacity);
}
