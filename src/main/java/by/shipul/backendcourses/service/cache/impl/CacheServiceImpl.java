package by.shipul.backendcourses.service.cache.impl;

import by.shipul.backendcourses.cache.CacheInMemory;
import by.shipul.backendcourses.cache.CacheType;
import by.shipul.backendcourses.cache.impl.LfuCacheInMemoryImpl;
import by.shipul.backendcourses.cache.impl.LruCacheInMemoryByTimeImpl;
import by.shipul.backendcourses.config.CacheConfigProperties;
import by.shipul.backendcourses.service.cache.CacheService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * Read cache mode and return it object
 *
 * @author Andrey Shipul
 * @since 20.01.2021 10:08
 */
@Slf4j
@RequiredArgsConstructor
public class CacheServiceImpl<K,V> implements CacheService<K,V> {

    private final CacheConfigProperties cacheProperties;
    private CacheInMemory<K,V> cacheInstance = null;

    /**
     * Get cache instance from file or default
     * to remove
     * private static DAO cacheInstance;
     * and this method
     * invoke factory method : createCache()
     *
     * @return cache instance
     */
    @Override
    public CacheInMemory<K,V> getCacheInstance() {
        if (Objects.isNull(cacheInstance)) {
            cacheInstance = createCache();
        }
        return cacheInstance;
    }

    /**
     * Read file preferences and create new instance cache
     *
     * @return cache instance
     */
    private CacheInMemory<K,V> createCache() {
        CacheInMemory<K,V> cache;
        try {
            cache = newInstanceCache(cacheProperties.getType(),cacheProperties.getCapacity());
        } catch (IllegalArgumentException e) {
            log.error("InitPropertyFromFile : IOException" + e.getMessage());
            log.warn("CacheFactory create default cache :" + CacheType.LRU_WITH_TIMESTAMP +
                    " with capacity= 10");
            cache = newInstanceCache(CacheType.LRU_WITH_TIMESTAMP, 10);
        }
        return cache;

    }

    /**
     * Create new instance
     *
     * @param cacheType type
     * @param capacity  capacity > 0
     * @return cache inst.
     */
    @Override
    public CacheInMemory<K,V> newInstanceCache(CacheType cacheType, int capacity) {
        CacheInMemory<K,V> cacheInstance;
        switch (cacheType) {
            case LFU:
                cacheInstance = new LfuCacheInMemoryImpl<>(capacity);
                break;
            case LRU_WITH_TIMESTAMP:
                cacheInstance = new LruCacheInMemoryByTimeImpl<>(capacity);
                break;
            default:
                log.warn("invalid newInstanceCache with " + cacheType + " capatity=" + capacity);
                throw new IllegalArgumentException("Unexpected value: " + cacheType);
        }
        return cacheInstance;
    }

}
