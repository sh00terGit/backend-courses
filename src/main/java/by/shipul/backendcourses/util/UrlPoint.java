package by.shipul.backendcourses.util;

/**
 * All url in one enum
 */
public enum UrlPoint {

    /**
     * Url for 404
     */
    NOT_FOUND("/error/404"),
    /**
     * Url for log transactions
     */
    CHANGE_LOG("/log"),
    /**
     * Url for transactions with material entity
     */
    MATERIAL("/material"),
    /**
     * Url for get cache obj
     */
    API_CACHE("/cache");


    private final String path;
    private final String localhost = "http://localhost";

    UrlPoint(String strValue) {
        this.path = strValue;
    }

    public String getHttpUrl() {
        return localhost + path;
    }

    public String getPath() {
        return path;
    }
}
