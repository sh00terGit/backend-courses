package by.shipul.backendcourses.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.*;

/**
 * Util class for application initializer
 */
@Slf4j
public class ApplicationInitializerUtil {

    /**
     * Delimiter for split sql rows
     */
    private static final String SQL_DELIMITER = ";";
    /**
     * Replaceable word for schema in sql row
     */
    private static final String SQL_REGEX_SCHEMA = "\\$schema";
    /**
     * Replaceable word for increment value sequence
     */
    private static final String SQL_REGEX_INCREMENT = "\\$inc_val";
    /**
     * Replaceable word for start value sequence
     */
    private static final String SQL_REGEX_START = "\\$start_val";

    /**
     * Read all nodes in objects from json file
     *
     * @return list of nodes
     */
    @SneakyThrows
    public static ArrayList<ApplicationNode> readAllNodesFromJsonFile(ObjectMapper mapper, String nodes_data_file) {
        ArrayList<ApplicationNode> applicationNodes = new ArrayList<>((Arrays.asList(mapper.readValue(new FileReader(nodes_data_file), ApplicationNode[].class))));
        return applicationNodes;
    }

    /**
     * Read sql files (SQL with create table and SQL with insert data) and write in db by current node (by rule)
     *
     * @param node      current node
     * @param nodesSize all nodes size
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public static Set<String> getSqlStatementsForNode(ApplicationNode node,
                                                      Integer nodesSize,
                                                      String sql_table_file,
                                                      String sql_data_file) throws FileNotFoundException, SQLException {
        List<String> sqlFileNames = new ArrayList<>(Arrays.asList(sql_table_file, sql_data_file));
        Set<String> sqls = new LinkedHashSet<>();
        for (String fileName : sqlFileNames) {
            Scanner myReader = new Scanner(new FileReader(fileName));
            myReader.useDelimiter(SQL_DELIMITER);
            long row = 0;
            while (myReader.hasNext()) {
                row++;
                String sql = myReader.next();
                sql = sql.replaceAll(SQL_REGEX_SCHEMA, node.getSchemaReplication());
                if (sql_table_file.endsWith(fileName)) {
                    sql = sql.replaceAll(SQL_REGEX_START, node.getId().toString());
                    sql = sql.replaceAll(SQL_REGEX_INCREMENT, nodesSize.toString());
                }
                if (sql_data_file.endsWith(fileName) &&
                        !((((row + nodesSize) % nodesSize) + 1) == node.getId())) {
                    continue;
                }
                sqls.add(sql);
            }
        }
        return sqls;

    }

}
