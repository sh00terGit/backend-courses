package by.shipul.backendcourses.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationNode {

    private Long id;
    private String schema;
    private List<String> ports;
    private String currentPort;
    private String masterPort;

    public String getSchemaReplication() {
        String indexCurrentPort = String.valueOf(ports.indexOf(currentPort) + 1);
        return schema.trim() + "_v" + indexCurrentPort;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        if (obj.getClass().equals(this.getClass())) {
            ApplicationNode appObj = (ApplicationNode) obj;
            return this.getId().equals(appObj.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
