package by.shipul.backendcourses.checker;

public interface MessagedChecker {

    String MESSAGE_PORT_NOT_AVAILABLE = "NODE-%d ---> PORT %s NOT AVAILABLE";
    String MESSAGE_MASTER_PORT = "NODE-%d ---> MASTER PORT IS %s";

    /**
     * Checker scan changes with fixed in service delay
     */
    void scanChangesForSchedule();

    default String getMessagePortNotAvailable(Long nodeId, String port) {
        return String.format(MESSAGE_PORT_NOT_AVAILABLE, nodeId, port);
    }

    default String getMessageMasterPort(Long nodeId, String port) {
        return String.format(MESSAGE_MASTER_PORT, nodeId, port);
    }
}
