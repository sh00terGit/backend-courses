package by.shipul.backendcourses.checker.impl.changelog.executor.impl;

import by.shipul.backendcourses.checker.impl.changelog.executor.ChangeLogCheckerExecutor;
import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.util.UrlPoint;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
public class ChangeLogCheckerExecutorImpl implements ChangeLogCheckerExecutor {

    private final RestTemplate restTemplate;

    /**
     * execute changes for current application (synchronize) by
     * ChangeLog object ( from master)
     * supports commands ( post , put ,delete)
     * or throw IllegalStateException
     *
     * @param changeLog obj
     * @return
     */
    @Override
    public HttpStatus executeLog(String toPort, ChangeLog changeLog) {

        HttpMethod httpMethod = HttpMethod.valueOf(changeLog.getMethod());
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(UrlPoint.MATERIAL.getHttpUrl()).port(toPort);
        uriBuilder = (httpMethod.equals(HttpMethod.DELETE))
                ? uriBuilder.queryParam("id",changeLog.getJson())
                : uriBuilder;
        HttpEntity<String> httpEntity = getHttpEntityFromChangeLog(changeLog);
        return restTemplate.exchange(uriBuilder.toUriString(), httpMethod, httpEntity, Object.class).getStatusCode();
    }


    private HttpEntity<String> getHttpEntityFromChangeLog(ChangeLog changeLog) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("fromPort", changeLog.getMasterPort());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntity = new HttpEntity<>(changeLog.getJson(), httpHeaders);
        return httpEntity;
    }
}
