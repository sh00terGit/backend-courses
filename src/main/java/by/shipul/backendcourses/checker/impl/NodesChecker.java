package by.shipul.backendcourses.checker.impl;

import by.shipul.backendcourses.checker.MessagedChecker;
import by.shipul.backendcourses.filter.helper.RedirectHelper;
import by.shipul.backendcourses.util.ApplicationNode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Map;


@Service
@Slf4j
@EnableScheduling
@RequiredArgsConstructor
public class NodesChecker implements MessagedChecker {

    @Getter
    private final Map<ApplicationNode, String> nodesMasterPort;
    private final ApplicationNode currentNode;
    private final ArrayList<ApplicationNode> appNodes;
    private final RedirectHelper redirectHelper;

    @Override
    @Scheduled(fixedDelay = 30000)
    public void scanChangesForSchedule() {
        for (ApplicationNode node : appNodes) {
            nodesMasterPort.put(node, node.equals(currentNode)
                    ? currentNode.getMasterPort()
                    : getAvailablePort(node));
        }
    }

    private String getAvailablePort(ApplicationNode node) {
        for (String port : node.getPorts()) {
            try {
                redirectHelper.pingPort(port);
                log.info(getMessageMasterPort(node.getId(), port));
                return port;
            } catch (ResourceAccessException | HttpClientErrorException e) {
                log.debug(getMessagePortNotAvailable(node.getId(), port));
            }

        }
        return null;
    }
}
