package by.shipul.backendcourses.checker.impl.changelog.executor;

import by.shipul.backendcourses.entity.ChangeLog;
import org.springframework.http.HttpStatus;

public interface ChangeLogCheckerExecutor {

    /**
     * execute changes for current application (synchronize) by
     * ChangeLog object ( from master)
     * supports commands ( post , put ,delete)
     * or throw IllegalStateException
     *
     * @param changeLog obj
     * @return
     */
    HttpStatus executeLog(String port, ChangeLog changeLog);
}
