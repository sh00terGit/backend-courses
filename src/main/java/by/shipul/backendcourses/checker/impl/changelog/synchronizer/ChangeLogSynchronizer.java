package by.shipul.backendcourses.checker.impl.changelog.synchronizer;

public interface ChangeLogSynchronizer {


    /**
     * Synchronize rows with current
     *
     * @param fromPort port from get changes
     * @param fromRow  from row
     * @param toRow    to row
     * @return boolean success
     */
    boolean synchronizeRows(String fromPort, Long fromRow, Long toRow);
}
