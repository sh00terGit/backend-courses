package by.shipul.backendcourses.checker.impl;

import by.shipul.backendcourses.checker.MessagedChecker;
import by.shipul.backendcourses.checker.StatusApp;
import by.shipul.backendcourses.filter.helper.RedirectHelper;
import by.shipul.backendcourses.util.ApplicationNode;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that monitors health current application node
 */

@Slf4j
@Data
@Service
@EnableScheduling
@RequiredArgsConstructor
public class HealthChecker implements MessagedChecker {

    private final ApplicationNode currentNode;
    private final RedirectHelper redirectHelper;
    private List<String> availablePorts;
    private StatusApp statusApp = StatusApp.RED;

    @Override
    @Scheduled(fixedDelay = 30000)
    public void scanChangesForSchedule() {
        availablePorts = scanNodePorts();
        checkStatus();
    }

    /**
     * Change status by available port size
     */
    private void checkStatus() {
        if (availablePorts.size() == currentNode.getPorts().size()) {
            statusApp = StatusApp.GREEN;
        } else if (availablePorts.size() == currentNode.getPorts().size() - 1) {
            statusApp = StatusApp.YELLOW;
        } else {
            statusApp = StatusApp.RED;
        }
        log.info("STATUS NODE : " + statusApp);
    }

    /**
     * Ping application node ports and add it if accessible
     *
     * @return list accessible ports
     */
    private List<String> scanNodePorts() {
        List<String> accessible = new ArrayList<>();

        for (String port : currentNode.getPorts()) {
            if (port.equals(currentNode.getCurrentPort())) {
                accessible.add(port);
                continue;
            }
            try {
                redirectHelper.pingPort(port);
                accessible.add(port);
            } catch (ResourceAccessException | HttpClientErrorException e) {
                log.error(getMessagePortNotAvailable(currentNode.getId(), port));
            }
        }
        return accessible;
    }

    public synchronized List<String> getAvailablePorts() {
        return availablePorts;
    }


}
