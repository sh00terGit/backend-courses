package by.shipul.backendcourses.checker.impl.changelog.synchronizer.impl;

import by.shipul.backendcourses.checker.impl.changelog.executor.ChangeLogCheckerExecutor;
import by.shipul.backendcourses.checker.impl.changelog.synchronizer.ChangeLogSynchronizer;
import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.util.UrlPoint;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ChangeLogSynchronizerImpl implements ChangeLogSynchronizer {

    /**
     * Start port application
     */
    @Value("${server.port}")
    private String currentPort;
    private final ChangeLogCheckerExecutor executor;
    private final RestTemplate restTemplate;

    /**
     * Synchronize rows with current
     *
     * @param fromPort port from get changes
     * @param fromRow  from row
     * @param toRow    to row
     * @return boolean success
     */
    @Override
    public boolean synchronizeRows(String fromPort, Long fromRow, Long toRow) {
        long successRow = 0;
        if (fromRow < toRow) {
            List<ChangeLog> listChanges = selectChangesMasterFromRow(fromPort, fromRow);
            if (Objects.nonNull(listChanges)) {
                for (ChangeLog logRow : listChanges) {
                    if (executor.executeLog(currentPort, logRow).is2xxSuccessful()) {
                        successRow++;
                    }
                }
            }
        }
        return (fromRow + successRow) == toRow;
    }

    /**
     * Makes a request to the master for all lines from the current
     *
     * @param fromRow
     * @return list ChangeLog objects
     */
    private List<ChangeLog> selectChangesMasterFromRow(String fromPort, Long fromRow) {
        String redirectURL = UriComponentsBuilder.fromHttpUrl(UrlPoint.CHANGE_LOG.getHttpUrl())
                .port(fromPort)
                .path("/changes")
                .queryParam("from", fromRow)
                .toUriString();
        ResponseEntity<List<ChangeLog>> exchange = restTemplate.exchange(redirectURL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ChangeLog>>() {
                });
        if (exchange.getStatusCode() == HttpStatus.OK) {
            return exchange.getBody();
        }
        return null;
    }

}
