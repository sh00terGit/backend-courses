package by.shipul.backendcourses.checker.impl.changelog;


import by.shipul.backendcourses.checker.MessagedChecker;
import by.shipul.backendcourses.checker.impl.HealthChecker;
import by.shipul.backendcourses.checker.impl.changelog.synchronizer.ChangeLogSynchronizer;
import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.util.ApplicationNode;
import by.shipul.backendcourses.util.UrlPoint;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.util.Objects;

/**
 * Service for synchronize changes between apps in one node
 */

@Slf4j
@Service
@EnableScheduling
@RequiredArgsConstructor
public class ChangeLogChecker implements MessagedChecker {

    private final ApplicationNode currentNode;
    private final HealthChecker healthChecker;
    private final RestTemplate restTemplate;
    private final ChangeLogSynchronizer synchronizer;
    /**
     * Start port application
     */
    @Value("${server.port}")
    private String currentPort;

    /**
     * Last fetched row from master
     */
    private Long lastMasterRow = 0L;

    /**
     * Last added current app row
     */
    private Long lastCurrentRow = 0L;

    /**
     * Construct checker and set current port as master port
     */
    @PostConstruct
    private void initMaster() {
        currentNode.setMasterPort(currentPort);
    }

    /**
     * Main logic method
     * interrogate available ports and get changes from master port every 30 seconds
     * for current port sets lastcurrentrow value
     */
    @Override
    @Scheduled(fixedDelay = 30000)
    public void scanChangesForSchedule() {
        if (Objects.isNull(healthChecker.getAvailablePorts())) {
            return;
        }
        for (String port : healthChecker.getAvailablePorts()) {
            try {
                ChangeLog lastLog = getLastRow(port).getBody();
                if (Objects.nonNull(lastLog)) {
                    lastMasterRow = (lastLog.getId() > lastMasterRow) ? lastLog.getId() : lastMasterRow;
                    lastCurrentRow = port.equals(currentPort) ? lastLog.getId() : lastCurrentRow;
                    if (lastMasterRow > lastCurrentRow) {
                        currentNode.setMasterPort(port);
                        if (synchronizer.synchronizeRows(port, lastCurrentRow, lastMasterRow)) {
                            lastCurrentRow = lastMasterRow;
                        }
                    }
                }
            } catch (ResourceAccessException | HttpClientErrorException e) {
                log.debug(getMessagePortNotAvailable(currentNode.getId(), port));
            }
        }
        log.info(getMessageMasterPort(currentNode.getId(), currentNode.getMasterPort()));
    }

    /**
     * Get's last row from rest request in application by port
     *
     * @param port
     * @return
     * @throws ResourceAccessException
     */
    private ResponseEntity<ChangeLog> getLastRow(String port) throws ResourceAccessException {
        String redirectURL = UriComponentsBuilder.fromHttpUrl(UrlPoint.CHANGE_LOG.getHttpUrl())
                .path("/last")
                .port(port)
                .toUriString();
        return restTemplate.getForEntity(redirectURL, ChangeLog.class);

    }


}
