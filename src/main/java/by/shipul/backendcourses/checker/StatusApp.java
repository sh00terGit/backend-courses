package by.shipul.backendcourses.checker;

/**
 * DevOps feature
 * which means status app
 * Green - normal
 * Yelllow - worked , but not full
 * Red - critical status
 */
public enum StatusApp {

    GREEN,
    YELLOW,
    RED
}
