package by.shipul.backendcourses.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Andrey Shipul
 * @since 21.01.2021 15:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "MEASURE")
public class Measure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

}
