package by.shipul.backendcourses.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Andrey Shipul
 * @since 21.01.2021 15:24
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MATERIAL")
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_GROUP", nullable = false)
    private GroupMaterial group;

    @ManyToOne
    @JoinColumn(name = "ID_MEASURE", nullable = false)
    private Measure measure;
    private String name;
    private String country;
}
