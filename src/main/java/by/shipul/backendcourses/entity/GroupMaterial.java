package by.shipul.backendcourses.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Andrey Shipul
 * @since 21.01.2021 15:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "GROUP_MATERIAL")
public class GroupMaterial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "GROUP_CODE")
    private String groupCode;

    @Column(name = "TYPE_DIRECTORY")
    private int typeDirectory;
}
