package by.shipul.backendcourses.filter.helper;

import by.shipul.backendcourses.checker.impl.HealthChecker;
import by.shipul.backendcourses.checker.impl.NodesChecker;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.util.ApplicationNode;
import by.shipul.backendcourses.util.UrlPoint;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class DistributionRuleHelper {

    private final NodesChecker nodesChecker;
    private final HealthChecker healthChecker;
    private final RedirectHelper redirectHelper;

    /**
     * Application rule distribution data by  nodes size and row id
     *
     * @param nodesSize all nodes size
     * @param id        id row
     * @return node id
     */
    private Long getNodeIdByEntityId(Integer nodesSize, Long id) {
        return (id % nodesSize) == 0 ? nodesSize : (id % nodesSize);
    }

    /**
     * Change node id by
     *
     * @param httpReq
     * @return
     */
    private Long getNodeIdByRequestBody(HttpServletRequest httpReq) {
        Material reqMaterial = redirectHelper.getMaterialFromHttpRequest(httpReq);
        Long val = Optional.ofNullable(reqMaterial.getId()).orElse((long) reqMaterial.hashCode());
        return Math.abs(getNodeIdByEntityId(nodesChecker.getNodesMasterPort().size(), val));
    }

    public String getNodeMasterPortByRequest(HttpServletRequest httpReq) {
        Long nodeId = (httpReq.getMethod().equals(HttpMethod.GET.toString()) || httpReq.getMethod().equals(HttpMethod.DELETE.toString()))
                ? getNodeIdByEntityId(nodesChecker.getNodesMasterPort().size(), Long.parseLong(httpReq.getParameter("id")))
                : getNodeIdByRequestBody(httpReq);
        ApplicationNode appNode = nodesChecker.getNodesMasterPort().keySet().stream()
                .filter(node -> node.getId().equals(nodeId))
                .findFirst()
                .get();
        return nodesChecker.getNodesMasterPort().get(appNode);
    }

    public Optional<ResponseEntity<Material>> findInCache(HttpServletRequest httpReqCached) {
        String appPort = String.valueOf(httpReqCached.getLocalPort());
        for (String port : healthChecker.getAvailablePorts()) {
            if (appPort.equals(port)) {
                continue;
            }
            String redirectUrl = UriComponentsBuilder.fromHttpUrl(UrlPoint.API_CACHE.getHttpUrl())
                    .port(port)
                    .queryParam("id", httpReqCached.getParameter("id"))
                    .toUriString();
            try {
                ResponseEntity<Material> restEntity = redirectHelper.getExchange(HttpMethod.GET, redirectUrl, null);
                if (restEntity.hasBody()) {
                    return Optional.of(restEntity);
                }
            } catch (ResourceAccessException e) {
                log.debug("findInCache url " + redirectUrl + " not available ");
            }
        }
        return Optional.empty();
    }
}
