package by.shipul.backendcourses.filter.helper;

import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.util.UrlPoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RedirectHelper {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    /**
     * If destination is application port then chain do ,
     * or else send request in destination application for result
     * if application is not available - 404
     *
     * @param httpReq         Request obj
     * @param httpResp        Response obj
     * @param chain           Chain Filter
     * @param destinationPort Destination port
     * @throws IOException
     * @throws ServletException
     */
    public void execOrRequestInOther(HttpServletRequest httpReq,
                                     HttpServletResponse httpResp,
                                     FilterChain chain,
                                     String destinationPort) throws IOException, ServletException {
        Material reqMaterial = getMaterialFromHttpRequest(httpReq);
        String appPort = String.valueOf(httpReq.getLocalPort());

        HttpMethod httpMethod = HttpMethod.valueOf(httpReq.getMethod());
        if (appPort.equals(destinationPort)) {
            chain.doFilter(httpReq, httpResp);
            return;
        }
        ResponseEntity<Material> responseEntity = getResponseEntity(httpMethod,
                destinationPort,
                httpReq,
                reqMaterial);

        if (Objects.isNull(destinationPort)) {
            httpReq.getServletContext().getRequestDispatcher(UrlPoint.NOT_FOUND.getPath()).forward(httpReq, httpResp);
            return;
        }
        setResponseValues(httpResp, responseEntity);
    }

    /**
     * Set response body by rest response by.shipul.backendcourses.entity
     *
     * @param httpResp
     * @param responseEntity
     * @return
     * @throws IOException
     */
    public HttpServletResponse setResponseValues(HttpServletResponse httpResp, ResponseEntity<Material> responseEntity) throws IOException {
        String entityJson = objectMapper.writeValueAsString(responseEntity.getBody());
        httpResp.setStatus(responseEntity.getStatusCodeValue());
        httpResp.getWriter().print(entityJson);
        return httpResp;
    }

    /**
     * Return Material class object from request or null
     *
     * @param httpReq HttpRequest
     * @return
     */
    public Material getMaterialFromHttpRequest(HttpServletRequest httpReq) {
        Material material = null;
        try {
            material = objectMapper.readValue(httpReq.getReader(), Material.class);
        } finally {
            return material;
        }

    }

    /**
     * Make a rest template request by url and return responce entity Material
     *
     * @param httpMethod
     * @param redirectURL
     * @param reqMaterial
     * @return
     */
    public ResponseEntity<Material> getExchange(HttpMethod httpMethod, String redirectURL, Material reqMaterial) {
        return restTemplate.exchange(redirectURL, httpMethod, new HttpEntity<>(reqMaterial), Material.class);
    }

    /**
     * Forward get request in another application and return response
     *
     * @param httpMethod      method to be applyed
     * @param destinationPort port for url
     * @param httpReq         HttpServletRequest
     * @param reqMaterial     requestEntity
     * @return responce material
     */
    private ResponseEntity<Material> getResponseEntity(HttpMethod httpMethod,
                                                       String destinationPort,
                                                       HttpServletRequest httpReq,
                                                       Material reqMaterial) {
        if (Objects.isNull(destinationPort)) return null;
        String redirectURL = getRedirectUrl(httpReq, destinationPort);
        return getExchange(httpMethod, redirectURL, reqMaterial);
    }

    /**
     * Get destination URL (same as current) but with changed port (by HttpServletRequest)
     *
     * @param httpReq
     * @param destinationPort
     * @return
     */
    private String getRedirectUrl(HttpServletRequest httpReq, String destinationPort) {
        return getRedirectUrl(getCurrentUrl(httpReq), destinationPort);
    }

    /**
     * Get destination URL (same as current) but with changed port
     *
     * @return
     */
    private String getRedirectUrl(String url, String destinationPort) {
        return UriComponentsBuilder.fromHttpUrl(url)
                .port(destinationPort)
                .toUriString();
    }

    /**
     * Get current url of HttpServletRequest (with  query string if available)
     *
     * @param httpReq HttpServletRequest
     * @return
     */
    private String getCurrentUrl(HttpServletRequest httpReq) {
        return (Optional.ofNullable(httpReq.getQueryString()).isPresent())
                ? httpReq.getRequestURL().append("?").append(httpReq.getQueryString()).toString()
                : httpReq.getRequestURL().toString();
    }

    /**
     * Dead or alive port
     *
     * @param port
     * @return
     */
    public ResponseEntity<Object> pingPort(String port) {
        String redirectURL = UriComponentsBuilder.fromHttpUrl(UrlPoint.CHANGE_LOG.getHttpUrl())
                .port(port)
                .toUriString();
        return restTemplate.exchange(redirectURL, HttpMethod.HEAD, null, Object.class);

    }

    /**
     * If it request is copy then chain, else continue filter
     *
     * @param httpReqCached request
     * @param httpResp      response
     * @param chain         chain
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public Boolean isCopyReplicRow(HttpServletRequest httpReqCached,
                                   HttpServletResponse httpResp,
                                   FilterChain chain) throws IOException, ServletException {
        String appPort = String.valueOf(httpReqCached.getLocalPort());
        Optional<String> fromPort = Optional.ofNullable(httpReqCached.getHeader("fromPort"));
        if (fromPort.isPresent()) {
            log.info("FILTER: COPY ROW FROM MASTERPORT " + fromPort.get());
            if (!appPort.equals(fromPort.get())) {
                chain.doFilter(httpReqCached, httpResp);
                return true;
            }
            return false;
        }
        return false;
    }
}

