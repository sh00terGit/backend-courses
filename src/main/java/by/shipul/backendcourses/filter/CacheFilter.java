package by.shipul.backendcourses.filter;

import by.shipul.backendcourses.checker.StatusApp;
import by.shipul.backendcourses.checker.impl.HealthChecker;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.filter.helper.DistributionRuleHelper;
import by.shipul.backendcourses.filter.helper.RedirectHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Filter for redirect request if necessary
 */
@Profile("!test")
@Slf4j
@WebFilter("/material")
@RequiredArgsConstructor
@Order(1)
public class CacheFilter extends WebHttpFilter {

    private final HealthChecker healthChecker;
    private final DistributionRuleHelper distributionRuleHelper;
    private final RedirectHelper redirectHelper;

    @Override
    public void doWebFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (healthChecker.getStatusApp() != StatusApp.RED &&
                request.getMethod().equals(HttpMethod.GET.toString())) {
            Optional<ResponseEntity<Material>> cacheEntity = distributionRuleHelper.findInCache(request);
            if (cacheEntity.isPresent()) {
                redirectHelper.setResponseValues(response, cacheEntity.get());
                return;
            }
        }
        chain.doFilter(request, response);

    }

}

