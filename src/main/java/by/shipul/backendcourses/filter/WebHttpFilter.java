package by.shipul.backendcourses.filter;

import by.shipul.backendcourses.filter.wrapper.CachedBodyHttpServletRequest;
import org.springframework.http.MediaType;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class WebHttpFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReqCached = new CachedBodyHttpServletRequest((HttpServletRequest) request);
        HttpServletResponse httpResp = (HttpServletResponse) response;
        httpResp.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpResp.setCharacterEncoding("UTF-8");
        doWebFilter(httpReqCached, httpResp, chain);
    }

    public abstract void doWebFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException;
}
