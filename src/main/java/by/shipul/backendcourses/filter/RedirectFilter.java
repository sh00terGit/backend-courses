package by.shipul.backendcourses.filter;

import by.shipul.backendcourses.filter.helper.DistributionRuleHelper;
import by.shipul.backendcourses.filter.helper.RedirectHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for redirect request if necessary
 */
@Profile("!test")
@Slf4j
@WebFilter("/material")
@RequiredArgsConstructor
@Order(2)
public class RedirectFilter extends WebHttpFilter {

    private final DistributionRuleHelper distributionRuleHelper;
    private final RedirectHelper redirectHelper;

    @Override
    public void doWebFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (redirectHelper.isCopyReplicRow(request, response, chain)) return;
        String nodeMasterPort = distributionRuleHelper.getNodeMasterPortByRequest(request);
        redirectHelper.execOrRequestInOther(request, response, chain, nodeMasterPort);
    }

}

