package by.shipul.backendcourses.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for 404 page
 */
@RestController
@RequestMapping("error/404")
@Slf4j
public class NotFoundController {

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    public ResponseEntity<String> get404() {
        return ResponseEntity.ok("404 NOT FOUND - RESOURCE ARE NOT AVAILABLE NOW.....");
    }
}
