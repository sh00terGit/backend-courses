package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.service.entity.ChangeLogEntityService;
import by.shipul.backendcourses.service.entity.impl.ChangeLogEntityServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Controller for service operations and crud logs
 */
@Slf4j
@RestController
@RequestMapping("log")
@RequiredArgsConstructor
public class ChangeLogController {

    private final ChangeLogEntityService service;

    /**
     * Get data last row
     * @return ChangeLog
     */
    @GetMapping("/last")
    public ResponseEntity<ChangeLog> getLastRow() {
                return ResponseEntity.ok(service.getLast());
    }

    /**
     * Get's changeLog changes from row in db
     * @param fromRow id
     * @return List<ChangeLog>
     */
    @GetMapping("/changes")
    public ResponseEntity<List<ChangeLog>> getChangesFromRow(@RequestParam(name = "from") long fromRow) {
                return ResponseEntity.ok(service.getChangesFromRow(fromRow));
    }

    /**
     * Ping method
     */
    @RequestMapping(method = RequestMethod.HEAD)
    public ResponseEntity<?> isAlive() {
        return ResponseEntity.ok().build();
    }

}
