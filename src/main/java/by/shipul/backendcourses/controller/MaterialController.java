package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.annotation.ChangeLogMethod;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import by.shipul.backendcourses.service.entity.EntityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("material")
@RequiredArgsConstructor
@Slf4j
public class MaterialController {

    private final EntityService<Material, Long> service;

    @GetMapping
    public ResponseEntity<Material> getEntityById(
            @RequestParam(name = "id") Long id) throws NotFoundServiceException {
        return ResponseEntity.ok(service.getEntity(id));
    }

    @ChangeLogMethod
    @PostMapping
    public ResponseEntity<Material> saveEntity(
            @RequestBody Material request,
            @RequestHeader(value = "fromPort",required = false) String port) throws InternalServiceException {
        Material material = service.saveEntity(request);
        return ResponseEntity.ok(material);
    }

    @ChangeLogMethod
    @PutMapping
    public ResponseEntity<Material> updateEntity(
            @RequestBody Material request,
            @RequestHeader(value = "fromPort", required = false) String port) throws InternalServiceException {
        Material material = service.updateEntityById( request);
        return ResponseEntity.ok(material);
    }

    @ChangeLogMethod
    @DeleteMapping
    public ResponseEntity<?> deleteEntityById(
            @RequestParam(name = "id") Long id,
            @RequestHeader(value = "fromPort",required = false) String port) throws NotFoundServiceException {
        service.removeEntity(id);
        return ResponseEntity.ok().build();
    }
}
