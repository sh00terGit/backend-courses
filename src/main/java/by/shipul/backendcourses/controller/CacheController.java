package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.cache.CacheService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * Controller for get cache instance from system memory
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("cache")
@Slf4j
public class CacheController {

    /**
     * Embedded system cache
     */
    private final CacheService<Long,Material> cacheService;

    @SneakyThrows
    @GetMapping
    public ResponseEntity<Material> getCache(@RequestParam(name = "id") Long materialId) {
        log.info("find in cache:" + materialId);
        Material material = cacheService.getCacheInstance().get(materialId);
        return Objects.nonNull(material) ? ResponseEntity.ok(material) : ResponseEntity.noContent().build();
    }
}
