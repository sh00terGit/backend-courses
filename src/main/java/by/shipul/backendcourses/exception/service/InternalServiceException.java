package by.shipul.backendcourses.exception.service;

/**
 * 500 error http
 *
 * @author Andrey Shipul
 * @since 29.01.2021 15:59
 */
public class InternalServiceException extends RuntimeException {

    public InternalServiceException(String message) {
        super(message);
    }
}
