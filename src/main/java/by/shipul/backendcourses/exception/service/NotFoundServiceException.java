package by.shipul.backendcourses.exception.service;

/**
 * 404 error
 *
 * @author Andrey Shipul
 * @since 29.01.2021 15:59
 */
public class NotFoundServiceException extends RuntimeException {

    public NotFoundServiceException(String message) {
        super(message);
    }

    public NotFoundServiceException() {
    }
}
