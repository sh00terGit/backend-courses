package by.shipul.backendcourses.exception.handler;

import by.shipul.backendcourses.exception.service.InternalServiceException;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ControllerHandler {

    @ExceptionHandler(NotFoundServiceException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(NotFoundServiceException ex) {
        log.warn(ex.getMessage());
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(InternalServiceException.class)
    protected ResponseEntity<Object> handleEntityServiceException(InternalServiceException ex) {
        log.warn(ex.getMessage());
        return ResponseEntity.noContent().build();
    }

}
