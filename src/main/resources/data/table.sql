SET search_path TO public;
drop schema if exists "$schema" cascade;
create schema "$schema";

CREATE SEQUENCE "$schema".material_seq INCREMENT $inc_val START $start_val;
create table "$schema".measure
(
    id   serial not null
        constraint m_measure_pkey
            primary key,
    name varchar(20)
);

create table "$schema".group_material
(
    id             serial   not null
        constraint m_grm_pk
            primary key,
    type_directory smallint not null,
    group_code     char(3)  not null,
    name           varchar(64)
);



create table "$schema".material
(
    id         BIGINT NOT NULL DEFAULT nextval('"$schema".material_seq') UNIQUE PRIMARY KEY,
    name       varchar(255),
    country    varchar(10),
    id_group   integer
        references group_material
            on update cascade on delete restrict,
    id_measure integer
        references measure
            on update cascade on delete restrict
);

create table "$schema".change_log
(
    id          serial not null
        constraint change_log_pk
            primary key,
    method      varchar(50),
    json        varchar(255),
    master_port varchar(50)
);
create index fki_f_grm on "$schema".material (id_group);
create index fki_f_mat_edizm on "$schema".material (id_measure);

INSERT INTO "$schema".measure (name)
VALUES ('ШТ');
INSERT INTO "$schema".measure (name)
VALUES ('КУС');
INSERT INTO "$schema".measure (name)
VALUES ('КГ');
INSERT INTO "$schema".measure (name)
VALUES ('Т');
INSERT INTO "$schema".measure (name)
VALUES ('ПАР');
INSERT INTO "$schema".measure (name)
VALUES ('БНК');
INSERT INTO "$schema".measure (name)
VALUES ('М2');
INSERT INTO "$schema".measure (name)
VALUES ('П/М');
INSERT INTO "$schema".measure (name)
VALUES ('ПАЧ');
INSERT INTO "$schema".measure (name)
VALUES ('М');

INSERT INTO "$schema".group_material (type_directory, group_code, name)
VALUES (100, '01 ', 'Всякое');
INSERT INTO "$schema".group_material (type_directory, group_code, name)
VALUES (100, '02 ', 'Сигареты');
INSERT INTO "$schema".group_material (type_directory, group_code, name)
VALUES (100, '03 ', 'Карамель');
INSERT INTO "$schema".group_material (type_directory, group_code, name)
VALUES (100, '04 ', 'Печенье');
