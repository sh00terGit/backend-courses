package by.shipul.backendcourses;

import by.shipul.backendcourses.checker.impl.HealthChecker;
import by.shipul.backendcourses.checker.impl.NodesChecker;
import by.shipul.backendcourses.checker.impl.changelog.ChangeLogChecker;
import by.shipul.backendcourses.entity.GroupMaterial;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.entity.Measure;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class SpringBootTestConfig {

    @MockBean
    private HealthChecker healthChecker;

    @MockBean
    private NodesChecker nodesChecker;

    @MockBean
    private ChangeLogChecker changeLogChecker;

    @Bean
    protected Material testMaterial(){
        GroupMaterial groupMaterial = new GroupMaterial(1L, "Всякое", "001", 100);
        Measure measure = new Measure(1L, "ШТ");
        return new Material(1L, groupMaterial, measure,"тестовая модель", "USA");
    }
}

