package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.service.entity.ChangeLogEntityService;
import by.shipul.backendcourses.util.UrlPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ChangeLogController.class)
class ChangeLogControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChangeLogEntityService service;
    private ChangeLog lastChangeLog;

    @BeforeEach
    public void init() {
        lastChangeLog =ChangeLog.builder()
                .id(6L)
                .method("DELETE")
                .masterPort("6081")
                .json("12")
                .build();

    }

    @Test
    void getLastRowTestSuccessfully() throws Exception {
        Mockito.when(service.getLast()).thenReturn(lastChangeLog);
        mockMvc.perform(get(getUriChangeLogWithAdditionalPath("/last")))
                .andExpect(jsonPath("$.id").value(String.valueOf(lastChangeLog.getId())))
                .andExpect(jsonPath("$.method").value(String.valueOf(lastChangeLog.getMethod())))
                .andExpect(jsonPath("$.masterPort").value(String.valueOf(lastChangeLog.getMasterPort())))
                .andExpect(jsonPath("$.json").value(String.valueOf(lastChangeLog.getJson())))
                .andExpect(status().isOk());
    }

    @Test
    void getAllFromRow() throws Exception {
        long from_row = 4L;
        ChangeLog changeLog2 =ChangeLog.builder()
                .id(6L)
                .method("PUT")
                .masterPort("6081")
                .json("{id:10}")
                .build();
        List<ChangeLog> changeLogList = Arrays.asList(lastChangeLog,changeLog2);

        Mockito.when(service.getChangesFromRow(from_row)).thenReturn(changeLogList);
        mockMvc.perform(get(getUriChangeLogWithAdditionalPath("/changes"))
                .queryParam("from",String.valueOf(from_row)))
                .andExpect(jsonPath("$[0].id").value(String.valueOf(lastChangeLog.getId())))
                .andExpect(jsonPath("$[0].method").value(String.valueOf(lastChangeLog.getMethod())))
                .andExpect(jsonPath("$[0].masterPort").value(String.valueOf(lastChangeLog.getMasterPort())))
                .andExpect(jsonPath("$[0].json").value(String.valueOf(lastChangeLog.getJson())))
                .andExpect(jsonPath("$[1].id").value(String.valueOf(changeLog2.getId())))
                .andExpect(jsonPath("$[1].method").value(String.valueOf(changeLog2.getMethod())))
                .andExpect(jsonPath("$[1].masterPort").value(String.valueOf(changeLog2.getMasterPort())))
                .andExpect(jsonPath("$[1].json").value(String.valueOf(changeLog2.getJson())))
                .andExpect(status().isOk());
    }

    private String getUriChangeLogWithAdditionalPath(String path){
        return UriComponentsBuilder.fromHttpUrl(UrlPoint.CHANGE_LOG.getHttpUrl())
                .path(path)
                .toUriString();
    }
}