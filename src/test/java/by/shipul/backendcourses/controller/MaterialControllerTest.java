package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.entity.MaterialEntityService;
import by.shipul.backendcourses.util.UrlPoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MaterialController.class)
@Import(SpringBootTestConfig.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
class MaterialControllerTest {

    @MockBean
    private MaterialEntityService service;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Material material;

    @Test
    void getEntityById() throws Exception {
        Mockito.when(service.getEntity(material.getId())).thenReturn(material);
        mockMvc.perform(get(UrlPoint.MATERIAL.getPath())
                .queryParam("id", String.valueOf(material.getId())))
                .andExpect(jsonPath("$.id").value(String.valueOf(material.getId())))
                .andExpect(jsonPath("$.name").value(String.valueOf(material.getName())))
                .andExpect(jsonPath("$.country").value(String.valueOf(material.getCountry())))
                .andExpect(jsonPath("$.measure.id").value(String.valueOf(material.getMeasure().getId())))
                .andExpect(jsonPath("$.group.id").value(String.valueOf(material.getGroup().getId())))
                .andExpect(status().is2xxSuccessful())
                .andDo(
                        document("{class-name}/{method-name}",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                relaxedResponseFields(
                                        fieldWithPath("name")
                                                .type(JsonFieldType.STRING)
                                                .description("name of material"),
                                        fieldWithPath("measure")
                                                .type(JsonFieldType.OBJECT)
                                                .description("measure of material")
                                ),
//                                responseFields(
//                                   subsectionWithPath("*").description("material")
//                                ),
                                requestParameters(
                                        parameterWithName("id").description("identifier"))

                        ));
    }

    @Test
    void saveEntity() throws Exception {
        String jsonMat = mapper.writeValueAsString(material);
        Mockito.when(service.saveEntity(material)).thenReturn(material);
        mockMvc.perform(post(UrlPoint.MATERIAL.getPath())
                .content(jsonMat)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(String.valueOf(material.getId())))
                .andExpect(status().is2xxSuccessful())
                .andDo(
                        document("{class-name}/{method-name}",
                             preprocessRequest(prettyPrint()),
                             preprocessResponse(prettyPrint()),
                             requestBody(Collections.singletonMap("body",content().json(jsonMat)))));
    }

    @Test
    void updateEntity() throws Exception {
        Collections.emptySet();
        String jsonMat = mapper.writeValueAsString(material);
        material.setId(2L);
        Mockito.when(service.updateEntityById(material)).thenReturn(material);
        mockMvc.perform(put(UrlPoint.MATERIAL.getPath())
                .content(jsonMat)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void deleteEntityById() throws Exception {
        Mockito.when(service.getEntity(material.getId())).thenReturn(material);
        mockMvc.perform(delete(UrlPoint.MATERIAL.getPath())
                .queryParam("id", String.valueOf(material.getId())))
                .andExpect(status().is2xxSuccessful());
    }
}