package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.cache.CacheInMemory;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.cache.CacheService;
import by.shipul.backendcourses.util.UrlPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CacheController.class)
@Import(SpringBootTestConfig.class)
class CacheControllerTest {

    @Mock
    private CacheInMemory cache;

    @MockBean
    private CacheService service;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Material material;


    @BeforeEach
    public void configure() {
        Mockito.when(service.getCacheInstance()).thenReturn(cache);
    }

    @Test
    void getCacheById() throws Exception {
        Mockito.when(cache.get(material.getId())).thenReturn(material);
            mockMvc.perform(get(UrlPoint.API_CACHE.getPath())
                    .queryParam("id", String.valueOf(material.getId())))
                    .andExpect(jsonPath("$.id").value(String.valueOf(material.getId())))
                    .andExpect(status().is2xxSuccessful());

    }

    @Test
    void getCacheByIdAndGetError() throws Exception {
        mockMvc.perform(get(UrlPoint.API_CACHE.getPath())
                .queryParam("id", "5"))
                .andExpect(status().isNoContent());

    }

}