package by.shipul.backendcourses.controller;

import by.shipul.backendcourses.util.UrlPoint;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(NotFoundController.class)
class NotFoundControllerTest {

    @Autowired
    private MockMvc mockMvc;

   @MethodSource("getRequest")
   @ParameterizedTest(name = "{index} => requestBuilder={0}")
    public void get404Test(RequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder)
                .andExpect(content().string("404 NOT FOUND - RESOURCE ARE NOT AVAILABLE NOW....."))
                .andExpect(status().is2xxSuccessful());
    }

    private static Stream<RequestBuilder> getRequest() {
        return Stream.of(get(UrlPoint.NOT_FOUND.getPath()),
                post(UrlPoint.NOT_FOUND.getPath()),
                put(UrlPoint.NOT_FOUND.getPath()),
                delete(UrlPoint.NOT_FOUND.getPath()));
    }
}