package by.shipul.backendcourses.annotation.impl;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.cache.CacheInMemory;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.cache.CacheService;
import by.shipul.backendcourses.service.entity.MaterialEntityService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = SpringBootTestConfig.class)
class CacheableImplTest {

    @Autowired
    private CacheService cacheService;

    @SpyBean
    private MaterialEntityService materialEntityService;

    @Autowired
    private Material material;
    private CacheInMemory cache;

    @BeforeEach
    public void setUp() {
        cache = cacheService.getCacheInstance();
    }

    @AfterEach
    public void tearDown() {
        cache = null;
    }

    @Test
    void saveInCacheAndDB() throws Exception {
        Material newMaterial = material;
        newMaterial.setId(null);

        materialEntityService.saveEntity(newMaterial);
        assertNotNull(cache.get(newMaterial.getId()));
    }

    @Test
    void updateInCacheAndDB() throws Exception {
        String country = "Russia";

        material.setCountry(country);
        materialEntityService.updateEntityById(material);
        Material materialFromCache = (Material) cache.get(material.getId());
        assertEquals(country, materialFromCache.getCountry());
    }

    @Test
    void getFromCacheOrDBWithElementInCache() {
        Material newMaterial = material;
        newMaterial.setId(null);

        materialEntityService.saveEntity(newMaterial);
        materialEntityService.getEntity(newMaterial.getId());
        Mockito.verify(materialEntityService, Mockito.times(0))
                .getEntity(newMaterial.getId());
    }

    @Test
    void getFromCacheOrDBWithElementNoContainsInCache() {
        long idMaterial = 2L;

        materialEntityService.getEntity(2L);
        Mockito.verify(materialEntityService, Mockito.times(1))
                .getEntity(idMaterial);
    }

    @Test
    void removeFromCacheAndDB() throws Exception {
        Material newMaterial = material;
        newMaterial.setId(null);

        materialEntityService.saveEntity(newMaterial);
        long materialId = newMaterial.getId();
        assertNotNull(cache.get(materialId));
        materialEntityService.removeEntity(materialId);
        assertNull(cache.get(materialId));
    }
}