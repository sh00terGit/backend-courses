package by.shipul.backendcourses.annotation.impl;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.service.entity.ChangeLogEntityService;
import by.shipul.backendcourses.service.entity.MaterialEntityService;
import by.shipul.backendcourses.util.UrlPoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest(classes = SpringBootTestConfig.class)
@AutoConfigureMockMvc
class ChangeLogMethodImplTest {

    @Autowired
    private ChangeLogEntityService changeLogService;

    @MockBean
    private MaterialEntityService materialService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Material testMaterial;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void controllerDeleteEntityByIdAndAspectBeInterceptAfterSuccessfulReturn() throws Exception {
        long materialId = 1L;

        mockMvc.perform((delete(UrlPoint.MATERIAL.getPath())
                .param("id", String.valueOf(materialId))));
        ChangeLog lastChangeLog = getLastChangeLog();
        assertEquals(materialId, Long.valueOf(lastChangeLog.getJson()));
        assertEquals(HttpMethod.DELETE.toString(), lastChangeLog.getMethod());
    }

    @Test
    void controllerUpdateEntityByIdAndAspectBeInterceptAfterSuccessfulReturn() throws Exception {
        testMaterial.setCountry("Armenia");
        String jsonTestMaterial = objectMapper.writeValueAsString(testMaterial);

        Mockito.when(materialService.updateEntityById(testMaterial)).thenReturn(testMaterial);
        mockMvc.perform(put(UrlPoint.MATERIAL.getPath())
                .content(objectMapper.writeValueAsString(testMaterial))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
        ChangeLog lastChangeLog = getLastChangeLog();
        assertEquals(jsonTestMaterial, lastChangeLog.getJson());
        assertEquals(HttpMethod.PUT.toString(), lastChangeLog.getMethod());
    }

    @Test
    void controllerSaveEntityAndAspectBeInterceptAfterSuccessfulReturn() throws Exception {
          Material testMaterialWithoutId = testMaterial;
        testMaterialWithoutId.setId(null);
        String jsonTestMaterial = objectMapper.writeValueAsString(testMaterial);

        Mockito.when(materialService.saveEntity(testMaterialWithoutId)).thenReturn(testMaterial);
        mockMvc.perform(post(UrlPoint.MATERIAL.getPath())
                .content(objectMapper.writeValueAsString(testMaterialWithoutId))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
        ChangeLog lastChangeLog = getLastChangeLog();
        assertEquals(jsonTestMaterial, lastChangeLog.getJson());
        assertEquals(HttpMethod.POST.toString(), lastChangeLog.getMethod());
    }

    private ChangeLog getLastChangeLog() {
        return changeLogService.getLast();
    }
}