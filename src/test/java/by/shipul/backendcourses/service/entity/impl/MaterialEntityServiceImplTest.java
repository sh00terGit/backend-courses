package by.shipul.backendcourses.service.entity.impl;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import by.shipul.backendcourses.service.entity.EntityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = SpringBootTestConfig.class)
class MaterialEntityServiceImplTest {

    @Autowired
    private EntityService<Material, Long> service;
    private Material material;

    @BeforeEach
    public void configure() {
        material = service.getEntity(1L);
    }

    @Test
    public void getEntityById() {
        assertNotNull(material);
    }

    @Test
    void getFaultEntityByIdAndThrowException() {
        assertThrows(NotFoundServiceException.class, () -> service.getEntity(-5L));
    }

    @Test
    void saveEntity() {
        assertNotNull(service.saveEntity(material));
    }

    @Test
    void removeEntityById() {
        service.removeEntity(2L);
        assertThrows(NotFoundServiceException.class, () -> service.getEntity(2L));
    }
}