package by.shipul.backendcourses.service.entity.impl;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.entity.ChangeLog;
import by.shipul.backendcourses.entity.Material;
import by.shipul.backendcourses.exception.service.NotFoundServiceException;
import by.shipul.backendcourses.service.entity.EntityService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = SpringBootTestConfig.class)
class ChangeLogEntityServiceImplTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EntityService<ChangeLog, Long> changeLogService;

    @Autowired
    private Material material;

    @Test
    public void getByIdTest() {
        ChangeLog result = changeLogService.getEntity(1L);
        assertNotNull(result);
    }

    @Test
    void getFaultEntityByIdAndThrowException() {
        assertThrows(NotFoundServiceException.class, () -> changeLogService.getEntity(-5L));
    }

    @Test
    public void saveMaterialAndGetChangeLogEntityTest() throws JsonProcessingException {
        String json = objectMapper.writeValueAsString(material);
        ChangeLog changeLog = getChangeLog(json);
        ChangeLog newChangeLog = changeLogService.saveEntity(changeLog);
        assertNotNull(newChangeLog);

    }

    @Test
    public void removeEntityByIdTest() {
        changeLogService.removeEntity(2L);
        assertThrows(NotFoundServiceException.class, () -> changeLogService.getEntity(2L));
    }

    @Test
    public void updateEntityByIdTest() throws JsonProcessingException {
        String country = "BELARUS";
        material.setCountry(country);
        String json = objectMapper.writeValueAsString(material);
        ChangeLog changeLog = changeLogService.updateEntityById(getChangeLog(json));
        Material materialActual = objectMapper.readValue(changeLog.getJson(), Material.class);
        assertEquals(country, materialActual.getCountry());
    }

    private ChangeLog getChangeLog(String json) {
        return ChangeLog.builder()
                .json(json)
                .method("POST")
                .masterPort("6081")
                .build();
    }

}