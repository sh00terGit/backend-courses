package by.shipul.backendcourses.service.cache;

import by.shipul.backendcourses.SpringBootTestConfig;
import by.shipul.backendcourses.cache.CacheInMemory;
import by.shipul.backendcourses.cache.CacheType;
import by.shipul.backendcourses.cache.impl.LfuCacheInMemoryImpl;
import by.shipul.backendcourses.cache.impl.LruCacheInMemoryByTimeImpl;
import by.shipul.backendcourses.config.CacheConfigProperties;
import by.shipul.backendcourses.entity.Material;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootTestConfig.class)
class CacheServiceImplTest {

    @Autowired
    private CacheService<Long,Material> service;

    @MockBean
    private CacheConfigProperties cacheProperties;
    private final int CACHE_SIZE = 3;

    @Test
    void getCacheInstanceTest() {
        Mockito.when(cacheProperties.getType()).thenReturn(CacheType.LFU);
        Mockito.when(cacheProperties.getCapacity()).thenReturn(CACHE_SIZE);
        assertNotNull(service.getCacheInstance());
    }

    @Test
    void getCacheWithInvalidParamsAndGetDefaultTest() {
        Mockito.when(cacheProperties.getType()).thenReturn(CacheType.LFU);
        Mockito.when(cacheProperties.getCapacity()).thenReturn(-5);
        assertTrue(service.getCacheInstance() instanceof LruCacheInMemoryByTimeImpl, "you must have LruCacheInMemoryByTimeImpl with size=10");
    }

    @Test
    void testGetLfuCache() {
        CacheInMemory<Long,Material> cacheInMemory = service.newInstanceCache(CacheType.LFU, CACHE_SIZE);
        assertTrue(cacheInMemory instanceof LfuCacheInMemoryImpl);
    }

    @Test
    void testGetLruCache() {
        CacheInMemory<Long,Material> cacheInMemory = service.newInstanceCache(CacheType.LRU_WITH_TIMESTAMP, CACHE_SIZE);
        assertTrue(cacheInMemory instanceof LruCacheInMemoryByTimeImpl);
    }


}