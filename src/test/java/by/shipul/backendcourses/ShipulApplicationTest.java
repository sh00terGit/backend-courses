package by.shipul.backendcourses;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SpringBootTestConfig.class)
class ShipulApplicationTest {

    @Test
    void contextLoads() {
    }
}