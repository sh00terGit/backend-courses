package by.shipul.backendcourses.cache.impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LruCacheInMemoryByTimeImplTest {

    private final int START_CAPACITY = 3;
    LruCacheInMemoryByTimeImpl<Integer, String> lru = new LruCacheInMemoryByTimeImpl<>(START_CAPACITY);

    /**
     * Thread.sleep added for obviousness
     */
    @BeforeEach
    void setUp() throws InterruptedException {
        lru.put(1, "1");
        Thread.sleep(10);
        lru.put(2, "2");
        Thread.sleep(10);
        lru.put(3, "3");
        Thread.sleep(10);
        assertEquals(3, lru.size());
    }

    @AfterEach
    void tearDown() {
        lru.clear();
        assertTrue(lru.isEmpty());
    }

    @Test
    void checkConstructorWithIllegalCapacity() {
        Throwable thrown = assertThrows(IllegalArgumentException.class, () ->
                new LruCacheInMemoryByTimeImpl<>(-1));
        assertEquals("capacity <= 0", thrown.getMessage());
    }


    @Test
    void getIsset() {
        assertEquals("2", lru.get(2));
    }

    @Test
    void getNoIsset() {
        assertNull(lru.get(5));
        assertNull(lru.get(-1));
    }

    @Test
    void getNullAndNullPointerEx() {
        assertThrows(NullPointerException.class, () -> lru.get(null));
    }

    @Test
    void deleteExists() {
        lru.delete(1);
        assertEquals(2, lru.size());
    }

    @Test
    void deleteNullAndNullPointerEx() {
        assertThrows(NullPointerException.class, () -> lru.delete(null));
    }

    @Test
    void deleteNonExists() {
        lru.delete(5);
        assertEquals(3, lru.size());
    }

    @Test
    void putNewKey() {
        lru.put(4, "4");
        assertEquals(3, lru.size(), "put size expected MAX SIZE OF LRU");
        assertEquals("{2=2, 3=3, 4=4}", lru.toString());
    }

    @Test
    void putExistsKey() {
        lru.put(1, "0");
        assertEquals(3, lru.size());
        assertEquals("0", lru.get(1));
    }

    @Test
    void testToString() {
        assertEquals("{1=1, 2=2, 3=3}", lru.toString());
    }

    @Test
    void clearLru() {
        lru.clear();
        assertTrue(lru.isEmpty());
    }

    @Test
    void isEmpty() {
        assertFalse(lru.isEmpty());
        lru.clear();
        assertTrue(lru.isEmpty());
    }
}