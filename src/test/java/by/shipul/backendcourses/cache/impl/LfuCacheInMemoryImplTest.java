package by.shipul.backendcourses.cache.impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LfuCacheInMemoryImplTest {

    private final int START_CAPACITY = 3;
    private LfuCacheInMemoryImpl<Integer, String> lfu = new LfuCacheInMemoryImpl<>(START_CAPACITY);

    @BeforeEach
    void setUp() {
        lfu.put(1, "1");
        lfu.put(2, "2");
        lfu.put(3, "3");
        assertEquals(3, lfu.size());
    }

    @AfterEach
    void tearDown() {
        lfu.clear();
        assertTrue(lfu.isEmpty());
    }

    @Test
    void checkConstructorWithIllegalCapacity() {
        Throwable thrown = assertThrows(IllegalArgumentException.class, () ->
                new LruCacheInMemoryByTimeImpl<>(-1));
        assertEquals("capacity <= 0", thrown.getMessage());
    }


    @Test
    void getIsset() {
        assertEquals("2", lfu.get(2));
    }

    @Test
    void getNoIsset() {
        assertNull(lfu.get(5));
        assertNull(lfu.get(-1));
    }

    @Test
    void getNullAndNullPointerEx() {
        assertThrows(NullPointerException.class, () -> lfu.get(null));
    }

    @Test
    void deleteExists() {
        lfu.delete(1);
        assertEquals(2, lfu.size());
    }

    @Test
    void deleteNullAndNullPointerEx() {
        assertThrows(NullPointerException.class, () -> lfu.delete(null));
    }

    @Test
    void deleteNonExists() {
        lfu.delete(5);
        assertEquals(3, lfu.size());
    }

    @Test
    void putNewKey() {
        lfu.put(4, "4");
        assertEquals(3, lfu.size(), "put size expected MAX SIZE OF LRU");
        assertEquals("{2=Node [value=2, frequency=1]," +
                " 3=Node [value=3, frequency=1]," +
                " 4=Node [value=4, frequency=1]}", lfu.toString());
    }

    @Test
    void putExistsKey() {
        lfu.put(1, "0");
        assertEquals(3, lfu.size());
        assertEquals("0", lfu.get(1));
    }

    @Test
    void testToString() {
        assertEquals("{1=Node [value=1, frequency=1]," +
                        " 2=Node [value=2, frequency=1]," +
                        " 3=Node [value=3, frequency=1]}",
                lfu.toString());
    }

    @Test
    void clearLru() {
        lfu.clear();
        assertTrue(lfu.isEmpty());
    }

    @Test
    void isEmpty() {
        assertFalse(lfu.isEmpty());
        lfu.clear();
        assertTrue(lfu.isEmpty());
    }

}