package by.shipul.backendcourses.util;

import by.shipul.backendcourses.SpringBootTestConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = SpringBootTestConfig.class)
class ApplicationInitializerUtilTest {

    @Value("${node.file.path}")
    private String nodesDataFilePath;

    @Value("${node.datasource.schema}")
    private String sql_table_file;

    @Value("${node.datasource.data}")
    private String sql_data_file;

    @Autowired
    private ObjectMapper objectMapper;
    private final int NODES_SIZE = 3;

    @Test
    void readAllNodesFromJsonFile() {
        List<ApplicationNode> applicationNodes = ApplicationInitializerUtil.readAllNodesFromJsonFile(objectMapper, nodesDataFilePath);
        assertEquals(NODES_SIZE, applicationNodes.size());
    }

    @Test
    void getSqlStatementsForNode() throws FileNotFoundException, SQLException {
        ApplicationNode applicationNode = ApplicationNode.builder()
                .id(1L)
                .currentPort("6081")
                .schema("first_node")
                .ports(Arrays.asList("6081", "6092", "6093"))
                .build();
        ApplicationInitializerUtil.getSqlStatementsForNode(applicationNode, NODES_SIZE, sql_table_file, sql_data_file);
    }
}