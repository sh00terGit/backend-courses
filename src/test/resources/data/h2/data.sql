INSERT INTO measure (id,name)
    VALUES (1,'ШТ'),
           (2,'КУС'),
           (3,'КГ');

INSERT INTO group_material (id,type_directory, group_code, name)
    VALUES  (1,100, '01 ', 'Всякое'),
            (2,100, '02 ', 'Сигареты'),
            (3,100, '03 ', 'Карамель'),
            (4,100, '04 ', 'Печенье');

INSERT INTO material (id,name,country,id_group,id_measure)
    VALUES  (1,'Сигареты "KENT BLUE"', 'BY', 2, 1),
            (2,'Сигареты "KENT DOUBLE MIX"', 'RU', 2, 1),
            (3,'Сигареты "KENT FEEL AURUM"', 'UA', 2, 1),
            (4,'Сигареты "KENT NAVY BLUE"', 'BY', 2, 1),
            (5,'Сигареты "PALL MALL DEMI BLUE"', 'BY', 2, 1),
            (6,'Сигареты "PALL MALL NANO SILVER"', 'BY', 2, 1);

INSERT INTO change_log(id,method,json,master_port)
    VALUES (1,'DELETE',10,'6081'),
            (2,'PUT','{"id":7,"name":"cookie","country":"UA","group":{"id":4,"name":"Печенье","groupCode":"04 ","typeDirectory":100},"measure":{"id":3,"name":"КГ"}}','6081'),
            (3,'POST','{"id":37,"name":"note","country":"UA","group":{"id":4,"name":"11","groupCode":"04 ","typeDirectory":1},"measure":{"id":2,"name":"ШТ"}}','6081');