drop table if exists measure;
drop table if exists material;
drop table if exists group_material;
drop table if exists change_log;

create table measure
(
    id   serial not null,
    name varchar(20)
);

create table group_material
(
    id serial   not null,
    type_directory smallint not null,
    group_code     char(3)  not null,
    name           varchar(64)
);

create table material
(
    id         serial not null,
    name       varchar(255),
    country    varchar(10),
    id_group   integer,
    id_measure integer
);

create table change_log
(
    id          serial not null,
    method      varchar(50),
    json        varchar(255),
    master_port varchar(50)
);

