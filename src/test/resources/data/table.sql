SET search_path TO public;
drop schema if exists "$schema" cascade;
create schema "$schema";

CREATE SEQUENCE "$schema".m_mat_seq INCREMENT $inc_val START $start_val;
create table "$schema".m_edizm
(
    id_edizm   serial not null
        constraint m_edizm_pkey
            primary key,
    name_edizm varchar(10)
);

create table "$schema".m_grm
(
    id_grm serial   not null
        constraint m_grm_pk
            primary key,
    tspr   smallint not null,
    kodgr  char(3)  not null,
    namegr varchar(64)
);



create table "$schema".m_mat
(
    id_mat      BIGINT NOT NULL DEFAULT nextval('"$schema".m_mat_seq') UNIQUE PRIMARY KEY,
    nmat        varchar(255),
    abr_country varchar(10),
    id_grm      integer,
    --       constraint f_mat_gr
    --        references m_grm
    --        on update cascade on delete set null,
    id_edizm    integer
    --  constraint f_mat_edizm
    --       references m_edizm
);

create table "$schema".change_log
(
    id          serial not null
        constraint change_log_pk
            primary key,
    method      varchar(50),
    json        varchar(255),
    master_port varchar(50)
);

alter table "$schema".change_log
    owner to postgres;

create index fki_f_grm on "$schema".m_mat (id_grm);
create index fki_f_mat_edizm on "$schema".m_mat (id_edizm);

alter table "$schema".m_grm
    owner to postgres;
alter table "$schema".m_edizm
    owner to postgres;
alter table "$schema".m_mat
    owner to postgres;


INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('ШТ');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('КУС');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('КГ');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('Т');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('ПАР');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('БНК');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('М2');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('П/М');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('ПАЧ');
INSERT INTO "$schema".m_edizm (name_edizm)
VALUES ('М');

INSERT INTO "$schema".m_grm (tspr, kodgr, namegr)
VALUES (100, '01 ', 'Всякое');
INSERT INTO "$schema".m_grm (tspr, kodgr, namegr)
VALUES (100, '02 ', 'Сигареты');
INSERT INTO "$schema".m_grm (tspr, kodgr, namegr)
VALUES (100, '03 ', 'Карамель');
INSERT INTO "$schema".m_grm (tspr, kodgr, namegr)
VALUES (100, '04 ', 'Печенье');
