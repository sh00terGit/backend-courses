INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "KENT BLUE"', 'BY', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "KENT DOUBLE MIX"', 'RU', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "KENT FEEL AURUM"', 'UA', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "KENT NAVY BLUE"', 'BY', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "PALL MALL DEMI BLUE"', 'BY', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сигареты "PALL MALL NANO SILVER"', 'BY', 2, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Вкусняшка"', 'UA', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Доминик-сэндвич" вкус шоколада', 'UA', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Творожка"', 'UA', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Импрессо" с дроблёным арахисом (190г)', 'BY', 4, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сдобное "Песочник с начинкой малина"', 'RU', 4, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Вишнёвый фрэш"', 'RU', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье Овсяное', 'BY', 4, 4);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Мороженое "ЮККИ Сметанковый"  пломбир в печенье сахарном 80г.', 'BY', 4, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сахарное "Вафельное рассыпчатое"', 'RU', 4, 4);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сдобное "Трубочка"', 'UA', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сдобное "Печенье из Домашкино"', 'RU', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье Весенняя рапсодия с дробл.арахисом', 'BY', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье "Сахарное" (100г)', 'BY', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сахарное "MultiCake" с начинкой клубника-крем (195г)', 'UA', 4, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Печенье сахарное "MultiCake" с начинкой вишня-кокос (180г)', 'UA', 4, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Шоколад "Nestle" молочный кофе латте (90г)', 'RU', 1, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Шоколад "Nestle" молочный с лесным орехом (90г)', 'RU', 1, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Шоколад "Nestle" молочный с миндалём и изюмом (90г)', 'RU', 1, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Шоколад "Nestle" тёмный (90г)', 'RU', 1, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Творог "101" зерно+сливки 5% 130г. персик-абрикос', 'BY', 1, 9);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Консерва рыбная "Скумбрия натуральная" (185г)', 'UA', 1, 1);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сок "Одесский" томатный с мякотью и солью (0,95л)', 'UA', 1, 6);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Колбаса "Медовая" с/к салями б/с', 'BY', 1, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Конфеты "Желейные" с желейным корпусом и вкусом вишни глазированные', 'RU', 1, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Конфеты "Желейные" с желейным корпусом и вкусом зелёного яблока глазированные', 'RU', 1, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Йогуртный продукт "Fruttis. Сливочное лакомство" пастеризованный 5% (115г)', 'RU', 1, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Сыр плавл. Новая Дружба 55% 100г.', 'BY', 1, 4);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Кофейный напиток "Jacobs monarch" 3в1 (15г)', 'RU', 1, 6);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Томаты свежие', 'ES', 1, 3);
INSERT INTO "$schema".m_mat (nmat, abr_country, id_grm, id_edizm)
VALUES ('Карамель" Рошен-джус-микс" весов.', 'UA', 3, 3);
